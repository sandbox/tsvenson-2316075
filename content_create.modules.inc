<?php

/**
 * @file
 * Content create
 *
 * Implements hook_content_create_options() for several core modules.
 */



/**
 * Field module. Implements hook_content_create_options().
 */
function field_content_create_options($entity_type, $bundle_name) {
  $sync_fields = array();
  if ($bundle_name) {
    $instances = field_info_instances($entity_type, $bundle_name);
    foreach ($instances as $name => $instance) {
      $sync_fields[$name] = array(
        'title' => $instance['label'],
        'description' => $instance['description'],
        'field_name' => $instance['field_name'],
        'group' => 'fields',
      );
    }
  }
  return $sync_fields;
}

/**
 * Node module. Implements hook_content_create_options().
 *
function node_content_create_options($entity_type, $bundle_name) {
  if ($entity_type == 'node') {
    return array(
      'uid' => array('title' => t('Author')),
      'status' => array('title' => t('Status')),
      'created' => array('title' => t('Post date')),
      'promote' => array('title' => t('Promote')),
      'moderate' =>  array('title' => t('Moderate')),
      'sticky' => array('title' => t('Sticky')),
      'revision' =>  array('title' => t('Revision'), 'description' => t('Create also new revision for translations')),
    );
  }
}*/
